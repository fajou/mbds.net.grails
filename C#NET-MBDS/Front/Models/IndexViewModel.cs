﻿using System.Collections.ObjectModel;
using Front.Models.Infos;

namespace Front.Models
{
    public class IndexViewModel
    {
        public int Year { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public Collection<StateRemboursement> Statistiques { get; set; }
    }
}