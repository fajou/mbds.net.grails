﻿using System;

namespace Front.Models.Infos
{
    public class PretInfo
    {
        public Guid? ClientID { get; set; }

        public Guid? CompteID { get; set; }

        // public int? Duration { get; set; }

        public decimal? Values { get; set; }
        
        public string Description { get; set; }

        public string Durations
        {
            set
            {
                var durations = value?.Split("-");
                if (durations == null || durations.Length != 2) 
                    return;
                DateStart = Convert.ToDateTime(durations[0].Trim());
                DateEnding = Convert.ToDateTime(durations[1].Trim());
                if(Equals(DateStart, DateEnding))
                    throw new Exception("La durration ne peut pas inferieur ou egal a zero.");
            }
            get => string.Empty;
        }
        public DateTime DateStart { get; set; }
        
        public DateTime DateEnding { get; set; }
    }
}