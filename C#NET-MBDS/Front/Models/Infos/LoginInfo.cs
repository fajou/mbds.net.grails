﻿namespace Front.Models.Infos
{
    public class LoginInfo
    {
        public string UserEmail { get; set; }
        public string PassWord { get; set; }
    }
}