﻿using System.Collections.Generic;

namespace Front.Models.Infos
{
    public class BreadcrumbInfo
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public Dictionary<string, string> Chemins { get; set; } = new Dictionary<string, string>();
    }
}