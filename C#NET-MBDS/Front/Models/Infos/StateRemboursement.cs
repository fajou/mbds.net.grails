﻿using System;

namespace Front.Models.Infos
{
    public class StateRemboursement
    {
        public Guid Id { get; set; }
        public virtual string Month { get; set; }
        public int Year { get; set; }
        public decimal Valeurs { get; set; }
        public decimal TotalLefts { get; set; }
        public decimal TotalValues { get; set; }

        public virtual decimal Progression
        {
            get
            {
                if (TotalValues == 0)
                    return 0;
                return (Valeurs / TotalValues);
            }
        }
    }
}