﻿using System.Collections.Generic;
using System.ComponentModel;
using Front.Models.Base;

namespace Front.Models.Infos
{
    public class ClientInfo
    {
        [Bindable(false)]
        public string ErrorMessage { get; set; }
        public Client Client { get; set; }
        public decimal TotalPret { get; set; }
        public decimal TotalRemboursement { get; set; }
        public decimal TotalLeftRembousement { get; set; }
        public List<Compte> Comptes { get; set; }
        public List<Pret> Prets { get; set; }
        public List<RemboursementClient> Remboursements { get; set; }
    }
}