﻿namespace Front.Models.Infos
{
    public class ConfigurationInfo
    {
        public decimal ConfigValues { get; set; }
        public int DurationMin { get; set; }
        public int DurationMax { get; set; }
        public string DurationType { get; set; }
    }
}