using System;
using System.ComponentModel.DataAnnotations;

namespace Front.Models.Base
{
    public sealed class Client
    {
        public Guid Id { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        
        public DateTime? StartDate { get; set; }
        [StringLength(100)] 
        public string PassWord { get; set; }
        [StringLength(100)] 
        public string UserEmail { get; set; }
        [StringLength(50)] 
        public string Currency { get; set; }
    }
}