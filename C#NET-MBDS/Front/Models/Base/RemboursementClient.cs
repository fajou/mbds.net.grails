using System;

namespace Front.Models.Base
{
    public sealed class RemboursementClient
    {
        public Guid Id { get; set; }
        public Guid PretId { get; set; }
        public Guid ClientId { get; set; }
        public decimal TotalValues { get; set; }
        public decimal TotalLefts { get; set; }
        public Int64 Version { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Number { get; set; }
        public string Currency { get; set; }
        public int Duration { get; set; }

        public decimal Progression()
        {
            var res = (TotalValues - TotalLefts) * 100 / TotalValues;
            return Math.Round(res, 2);
        }
    }
}