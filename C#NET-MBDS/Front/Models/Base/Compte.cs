using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Front.Utils;

namespace Front.Models.Base
{
    public sealed class Compte
    {
        private static readonly Random _random = new Random();

        public Guid Id { get; set; }

        public Int64 Version { get; set; }

        [StringLength(20)] 
        public string Number { get; set; }

        public Guid? ClientID { get; set; }

        public decimal? Valeurs { get; set; }

        public string Currency { get; set; }
        
        public int Etat { get; set; }
        
        public static string GenerateCompteNumber()
        {
            var slotI = _random.Next(1000, 5000);
            var slotII = _random.Next(2000, 6000);
            var slotIII = _random.Next(7000, 9000);
            var slotIV = _random.Next(1000, 9000);
            
            return $"{slotI} {slotII} {slotIII} {slotIV}";
        }
        
        public static Compte NewCompte(Client client)
        {
            return new Compte
            {
                Id = Guid.NewGuid(),
                Version = 1,
                Number = GenerateCompteNumber(),
                ClientID = client.Id,
                Valeurs = 0,
                Etat = Etats.EN_COURS,
                Currency = CultureInfo.CurrentCulture.Name
            };
        }
    }
}