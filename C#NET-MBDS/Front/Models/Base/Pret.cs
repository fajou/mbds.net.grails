using System;
using System.ComponentModel.DataAnnotations;

namespace Front.Models.Base
{
    public sealed class Pret
    {
        public Guid Id { get; set; }
        public Guid? ClientID { get; set; }
        public Guid? CompteID { get; set; }
        public Guid? PretConfigID { get; set; }
        public int Duration { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Valeurs { get; set; }
        [StringLength(50)] 
        public string Currency { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Int64 Version { get; set; }
        public int Etat { get; set; }
    }
}