using System;

namespace Front.Models.Base
{
    public sealed class Remboursement
    {
        public Guid Id { get; set; }

        public Guid? PretId { get; set; }

        public Guid? ClientId { get; set; }

        public decimal? TotalValues { get; set; }

        public decimal? TotalLefts { get; set; }
        
        public Int64 Version { get; set; }
        
        public int Etat { get; set; }
    }
}