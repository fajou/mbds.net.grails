﻿using Front.Extensions;
using Front.Models.Base;
using Front.Models.Infos;
using Front.Services;
using Microsoft.AspNetCore.Mvc;

namespace Front.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientService clientService;
        
        public ClientController(IClientService clientService)
        {
            this.clientService = clientService;
        }

        public IActionResult Login(bool error)
        {
            var session = HttpContext.Session.Get<Client>(Names.ClientSessionName);
            if (session != null)
                return RedirectToAction("Index", "Front");

            if (error) 
                ViewBag.Error = "Invalide addresse email ou mots de passe.";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ValidationLogin(LoginInfo login)
        {
            var client = clientService.LoginClient(login.UserEmail, login.PassWord);
            if (client?.Id == null)
                return RedirectToAction("Login", new {error = true});
            
            HttpContext.Session.Clear();
            HttpContext.Session.Set(Names.ClientSessionName, client);
            
            return RedirectToAction("Index", "Front");
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ValidationRegister(RegisterInfo registerInfo)
        {
            return RedirectToAction("Index", "Front");
        }

        public IActionResult Deconnexion()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }
    }
}