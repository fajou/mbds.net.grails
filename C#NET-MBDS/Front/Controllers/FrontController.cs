﻿using System;
using Front.Extensions;
using Front.Models;
using Front.Models.Base;
using Front.Models.Infos;
using Front.Services;
using Microsoft.AspNetCore.Mvc;

namespace Front.Controllers
{
    public class FrontController : Controller
    {
        private readonly IClientService clientService;
        private readonly IPretService pretService;
        private readonly ICompteService compteService;

        public FrontController(IClientService clientService, IPretService pretService, ICompteService compteService)
        {
            this.clientService = clientService;
            this.pretService = pretService;
            this.compteService = compteService;
        }

        private ClientInfo DataCurrentClient()
        {
            var client = HttpContext.Session.Get<Client>(Names.ClientSessionName);
            return clientService.GetClientInfo(client.Id);
        }
        
        private void SetDataCurrentClient()
        {
            ViewData[Names.ClientSessionInfo] = DataCurrentClient();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Index(int? year)
        {
            SetDataCurrentClient();
            var clientInfo = DataCurrentClient();

            return View(new IndexViewModel
            {
                ClientInfo = clientInfo,
                Statistiques = clientService.GetStateRemboursements(clientInfo.Client, year ?? 2020),
                Year = year ?? 2020
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TraiterPreter(PretInfo info)
        {
            try
            {
                pretService.SavePret(info);
                return RedirectToAction("Index", "Front");
            }
            catch (Exception e)
            {
                SetDataCurrentClient();
                var model = DataCurrentClient();
                model.ErrorMessage = e.Message;
                return View("Index", new IndexViewModel
                {
                    ClientInfo = model,
                    Statistiques = clientService.GetStateRemboursements(model.Client, 2020),
                    Year = 2020
                });
            }
        }

        public IActionResult NewConfiguration(ConfigurationInfo info)
        {
            pretService.SaveConfig(info);
            return RedirectToAction("Index", "Front");
        }
        
        public IActionResult Privacy()
        {
            return View();
        }
    }
}