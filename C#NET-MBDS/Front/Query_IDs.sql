﻿create table Client
(
    Version   bigint           default 1,
    ID        uniqueidentifier default newid() not null primary key,
    Etat int default 0,
    LastName  varchar(100),
    FirstName varchar(100),
    StartDate datetime,
    PassWord  varchar(100),
    UserEmail varchar(100),
    Currency  varchar(50)
)
go

create table Compte
(
    Version  bigint           not null,
    ID       uniqueidentifier not null
        primary key,
    Etat     int default 0,
    Number   varchar(20),
    ClientID uniqueidentifier references dbo.Client,
    Valeurs  decimal(10, 2),
    Currency varchar(50)
)
go


create table PretConfig
(
    Version      int         default 1,
    Id           uniqueidentifier not null
        primary key,
    Etat int default 0,
    ConfigValues decimal(10, 2),
    DurationMin  int,
    DurationMax  int,
    DurationType varchar(10) default 'Days'
)
go

create table Pret
(
    Version     int default 1,
    ID          uniqueidentifier not null
        primary key,
        Etat int default    0,
    ClientID    uniqueidentifier
        references Client,
    CompteID    uniqueidentifier
        references Compte,
    PretConfigId    uniqueidentifier
        references PretConfig,
    Duration    int,
    Date        datetime,
    Valeurs     decimal(10, 2),
    Currency    varchar(50),
    Description text,
    StartDate   datetime         not null,
    EndDate     datetime         not null
)
go

create table Remboursement
(
    Version     bigint           not null,
    ID          uniqueidentifier not null primary key,
    Etat        int default 0,
    PretID      uniqueidentifier references Pret,
    ClientID    uniqueidentifier references Client,
    TotalValues decimal(10, 2),
    TotalLefts  decimal(10, 2)
)
go


create table RemboursementDetail
(
    Version  bigint           not null,
    ID       uniqueidentifier not null primary key,
    Etat     int default 0,
    ParentID uniqueidentifier references Remboursement
       ,
    Date     datetime,
    Interet  decimal(10, 2),
    Valeurs  decimal(10, 2)
)
go


create view RembsDetail as
select RD.ID,
       ParentID,
       R2.Etat,
       Date,
       Interet,
       Valeurs,
       RD.Version,
       PretID,
       ClientID,
       TotalValues,
       TotalLefts
from RemboursementDetail RD
         join Remboursement R2 on RD.ParentID = R2.ID;


create view RemboursementsClient as
select R2.*,
       P.Duration,
       P.StartDate,
       P.EndDate,
       C.Number,
       C.Currency
from Remboursement R2
         join Pret P on R2.PretID = P.ID
         join Compte C on P.CompteID = C.ID