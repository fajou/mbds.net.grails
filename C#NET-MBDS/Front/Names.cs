﻿namespace Front
{
    public static class Names
    {
        public const string ClientSessionInfo = ".ClientSessionName.Infos";
        public const string ClientSessionName = ".ClientSessionName.Values";
        public const string BreadcrumbInfo = ".Breadcrumb.Infos";
    }
}