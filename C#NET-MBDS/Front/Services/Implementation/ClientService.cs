﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Front.Models.Base;
using Front.Models.Infos;
using Front.Utils;

namespace Front.Services.Implementation
{
    public class ClientService : IClientService
    {
        private static readonly Random _random = new Random();
        private readonly ICompteService _compteService;
        private readonly IRemboursementService _remboursementService;
        private readonly IPretService _pretService;

        public ClientService(ICompteService compteService, IRemboursementService remboursementService, IPretService pretService)
        {
            _compteService = compteService;
            _remboursementService = remboursementService;
            _pretService = pretService;
        }

        public Client GetClient(Guid guid)
        {
            return DataBase.First(new Client {Id = guid}) as Client;
        }

        public Client SaveClient(Client client)
        {
            throw new NotImplementedException();
        }

        public List<Client> GetClients()
        {
            return DataBase.Find(new Client()).Cast<Client>().ToList();
        }

        public ClientInfo GetClientInfo(Guid guid)
        {
            if (!(DataBase.FindByColumnGuid(new Client(), nameof(Client.Id), guid) is Client client))
                return new ClientInfo();

            var clientInfo = new ClientInfo
            {
                Client = client,
                Comptes = _compteService.GetComptesClient(client.Id),
                Remboursements = _remboursementService.GetRemboursementClients(client),
                Prets = _pretService.GetPrets(client)
            };
            clientInfo.TotalPret = clientInfo.Remboursements.Sum(r => r.TotalValues);
            clientInfo.TotalLeftRembousement = clientInfo.Remboursements.Sum(r => r.TotalLefts);
            clientInfo.TotalRemboursement = clientInfo.TotalPret - clientInfo.TotalLeftRembousement;

            return clientInfo;
        }

        public Collection<StateRemboursement> GetStateRemboursements(Client client, int year)
        {
            var states = new Collection<StateRemboursement>();
            try
            {
                using (var conn = DbConnection.GetConnection())
                {
                    var format = new DateTimeFormatInfo();
                    for (var month = 1; month <= 12; month++)
                    {
                        var obj = new StateRemboursement();
                        var model = DataBase.FindQuery($"select * from StateRemboursement('{client.Id}', {year}, {month})", obj, conn)
                            .FirstOrDefault() as StateRemboursement ?? obj;
                        model.Month = format.MonthNames[month - 1];
                        states.Add(model);
                    }
                    return states;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static string Sha256Hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                return string.Concat(hash.ComputeHash(Encoding.UTF8.GetBytes(value)).Select(item => item.ToString("x2")));
            }
        }

        public Client LoginClient(string mail, string pass)
        {
            return DataBase.Find(new Client(), $"UserEmail='{mail}' and PassWord='{Sha256Hash(pass)}'")
                .FirstOrDefault() as Client;
        }

        public void GenerateAllCompteClient()
        {
            using (var conn = DbConnection.GetConnection())
            {
                var clients = GetClients();
                foreach (var compte in clients.Select(Compte.NewCompte))
                    DataBase.Save(compte, conn);
            }
        }

        public void GenerateManyPretsClient()
        {
            using (var conn = DbConnection.GetConnection())
            {
                var clients = DataBase.Find(new Client(), conn).Cast<Client>().ToList();
                var configs = DataBase.Find(new PretConfig(), conn).Cast<PretConfig>().ToList();

                foreach (var client in clients)
                {
                    var comptes = DataBase.Find(new Compte(), $"clientid='{client.Id}'").Cast<Compte>().ToList();
                    var n = _random.Next(1, 100);
                    for (var i = 0; i < n; i++)
                    {
                        foreach (var compte in comptes)
                        {
                            var duration = _random.Next(15, 61);
                            var values = _random.Next(50000, 1000000);
                            var start = new DateTime(_random.Next(2000, 2021), _random.Next(1, 13), _random.Next(1, 29));
                            var conf = configs.FirstOrDefault(config => config.DurationMin <= duration && duration <= config.DurationMax) ?? configs.Last();

                            var pret = new Pret
                            {
                                Id = Guid.NewGuid(),
                                ClientID = client.Id,
                                CompteID = compte.Id,
                                PretConfigID = conf.Id,
                                Version = 0,
                                Etat = Etats.EN_COURS,
                                Duration = duration,
                                Date = start,
                                Valeurs = values,
                                Currency = CultureInfo.CurrentCulture.Name,
                                Description = "Generated pret",
                                StartDate = start,
                                EndDate = start.AddDays(duration)
                            };
                            DataBase.Save(pret, conn);
                        }
                    }
                }
            }
        }

        public void GenerateManyRemboursement()
        {
            using (var conn = DbConnection.GetConnection())
            {
                var prets = DataBase.Find(new Pret(), conn).Cast<Pret>().ToList();
                foreach (var pret in prets)
                {
                    if (pret.StartDate >= DateTime.Now) 
                        continue;
                    
                    var compte = DataBase.Find(new Compte(), $"id='{pret.CompteID}'").First() as Compte ?? new Compte();
                    var config = DataBase.Find(new PretConfig(), $"id='{pret.PretConfigID}'").First() as PretConfig ?? new PretConfig();
                    var rembs = new Remboursement
                    {
                        Id = Guid.NewGuid(),
                        Etat = Etats.REMBOURSER,
                        Version = 1,
                        ClientId = pret.ClientID,
                        PretId = pret.Id,
                        TotalLefts = 0,
                        TotalValues = pret.Valeurs
                    };
                    compte.Valeurs -= pret.Valeurs;
                    pret.Etat = Etats.REMBOURSER;
                    
                    var interet = config.ConfigValues;
                    var durr = _random.Next(config.DurationMin, config.DurationMax);
                    var detailRembs = new RemboursementDetail
                    {
                        Id = Guid.NewGuid(),
                        Date = pret.StartDate.AddDays(durr),
                        Etat = Etats.REMBOURSER,
                        Interet = pret.Valeurs * interet,
                        ParentId = rembs.Id,
                        Valeurs = pret.Valeurs,
                        Version = 1
                    };
                    
                    DataBase.Update(compte, "Id", conn);
                    DataBase.Update(pret, "Id", conn);
                    DataBase.Save(rembs, conn);
                    DataBase.Save(detailRembs, conn);
                }
            }
        }
    }
}