﻿using System;
using System.Collections.Generic;
using System.Linq;
using Front.Models.Base;

namespace Front.Services.Implementation
{
    public class CompteService : ICompteService
    {
        public Compte GetCompte(Guid guid)
        {
            var o = DataBase.FindByColumnGuid(new Compte(), "Id", guid);
            return o as Compte;
        }

        public List<Compte> GetComptesClient(Guid clientGuid)
        {
            var o = DataBase.Find(new Compte(), $"ClientID='{clientGuid}'");
            var results = o.Cast<Compte>().ToList();
            return results;
        }

        public Compte CreateCompte(Client client, decimal? value)
        {
            var compte = Compte.NewCompte(client);
            compte.Valeurs = value;
            DataBase.Save(compte);
            return compte;
        }
    }
}