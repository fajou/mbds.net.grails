﻿using System;
using System.Data.SqlClient;

namespace Front.Services.Implementation
{
    internal static class DbConnection
    {
        public static string InitialCatalog;
        private const string StringConnection = "Data Source=PBIO-2694;Initial Catalog=BANK_INFO_DB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static void Close(SqlDataReader reader, SqlConnection connection)
        {
            reader?.Close();
            connection?.Close();
        }

        public static SqlConnection GetConnection()
        {
            try
            {
                var builder = new SqlConnectionStringBuilder(StringConnection);
                if (string.IsNullOrEmpty(InitialCatalog))
                    InitialCatalog = builder.InitialCatalog;
                else
                    builder.InitialCatalog = InitialCatalog;

                var conn = new SqlConnection(builder.ConnectionString);
                conn.Open();
                return conn;
            }
            catch (Exception e)
            {
                throw new Exception("Error de la connection. Cause :" + e.Message);
            }
        }

        public static SqlDataReader DataReader(string query, SqlConnection con)
        {
            Console.WriteLine(query);
            var result = new SqlCommand(query, con).ExecuteReader();
            return result;
        }

        public static void Execute(string query, SqlConnection connection)
        {
            try
            {
                var cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                Console.WriteLine(query);
            }
            catch (Exception ex)
            {
                throw new Exception("Error Cause : " + ex.Message);
            }
        }

        public static void Execute(string query)
        {
            SqlConnection connection = null;
            try
            {
                connection = GetConnection();
                Execute(query, connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error Cause : " + ex.Message);
            }
            finally
            {
                connection?.Close();
            }
        }
    }
}