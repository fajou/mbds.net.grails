﻿using System;
using System.Collections.Generic;
using System.Linq;
using Front.Models.Base;
using Front.Utils;

namespace Front.Services.Implementation
{
    public class RembousementService :IRemboursementService
    {
        public Remboursement GetRemboursement(Guid guid)
        {
            var o = DataBase.FindByColumnGuid(new Remboursement(), "Id", guid);
            return o as Remboursement;
        }

        public List<RemboursementDetail> GetRemboursementDetails(Guid remboursementID)
        {
            var o = DataBase.Find(new RemboursementDetail(), $"ParentId='{remboursementID}'");
            return o.Cast<RemboursementDetail>().ToList();
        }

        public List<Remboursement> GetRemboursements(Pret pret, Client client)
        {
            var o = DataBase.Find(new Remboursement(), $"PretId='{pret.Id}' and ClientId='{client.Id}'");
            return o.Cast<Remboursement>().ToList();
        }

        public List<Remboursement> GetRemboursements(Client client)
        {
            var where = $"ClientId='{client.Id}'";
            var o = DataBase.Find(new Remboursement(), where);
            return o.Cast<Remboursement>().ToList();
        }

        public List<RemboursementClient> GetRemboursementClients(Client client)
        {
            var where = $"ClientId='{client.Id}' and Etat={Etats.REMBOURSER} or Etat={Etats.EN_COURS}";
            var o = DataBase.Find(new RemboursementClient(), where);
            return o.Cast<RemboursementClient>().ToList();
        }
    }
}