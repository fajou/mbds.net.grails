﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Front.Services.Implementation
{
    internal static class DataBase 
    {
        public static string QueryDelete(object info, string columnID)
        {
            var query = $"DELETE {info.GetType().Name} WHERE {columnID} = '{info.GetType().GetProperty(columnID)}'";
            Console.WriteLine(query);
            return query;
        }

        public static string QueryDeleteAll(object info)
        {
            var query = $"DELETE {info.GetType().Name}";
            Console.WriteLine(query);
            return query;
        }

        public static string QueryList(object info, string where)
        {
            where = string.IsNullOrEmpty(where) ? "" : $"WHERE {where}";

            var query = $"SELECT * FROM {info.GetType().Name} {where}";
            Console.WriteLine(query);
            return query;
        }

        public static string QuerySave(object info)
        {
            var properties = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
            var nomColone = "";
            var valueColone = "";
            foreach (var property in properties)
            {
                nomColone += $"{property.Name}, ";
                valueColone += $"@{property.Name}, ";
            }

            nomColone = nomColone.Remove(nomColone.Length - 2);
            valueColone = valueColone.Remove(valueColone.Length - 2);
            var query = $"INSERT INTO {info.GetType().Name} ({nomColone}) VALUES ({valueColone})";
            Console.WriteLine(query);
            return query;
        }

        public static string QueryUpdate(object info, string columnID)
        {
            var propertyInfos = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
            var valueColonne = propertyInfos.Aggregate("", (current, property) => current + $"{property.Name} = @{property.Name}, ");

            valueColonne = valueColonne.Remove(valueColonne.Length - 2);
            var query = $"UPDATE {info.GetType().Name} SET {valueColonne}  WHERE {columnID} = '{info.GetType().GetProperty(columnID)?.GetValue(info)}'";
            Console.WriteLine(query);
            return query;
        }

        public static void Delete(object info, string columnID)
        {
            SqlConnection scon = null;
            try
            {
                scon = DbConnection.GetConnection();
                Delete(info, scon, columnID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, scon);
            }
        }

        public static void DeleteAll(object info, SqlConnection conn)
        {
            DbConnection.Execute(QueryDeleteAll(info), conn);
        }

        public static void DeleteAll(object info)
        {
            SqlConnection scon = null;
            try
            {
                scon = DbConnection.GetConnection();
                DeleteAll(info, scon);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, scon);
            }
        }

        public static void Delete(object info, SqlConnection conn, string columnID)
        {
            DbConnection.Execute(QueryDelete(info, columnID), conn);
        }

        public static void DeleteList(List<object> info, string columnID)
        {
            SqlConnection scon = null;
            try
            {
                scon = DbConnection.GetConnection();
                DeleteList(info, scon, columnID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, scon);
            }
        }

        public static void DeleteList(List<object> bms, SqlConnection conn, string columnID)
        {
            if (bms == null || bms.Count == 0) return;

            foreach (var info in bms) DbConnection.Execute(QueryDelete(info, columnID), conn);
        }

        public static void Save(object info)
        {
            SqlConnection connection = null;
            try
            {
                connection = DbConnection.GetConnection();
                Save(info, connection);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, connection);
            }
        }

        public static void Save(object info, SqlConnection conn)
        {
            var cmd = new SqlCommand(QuerySave(info), conn);
            var properties = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
            foreach (var property in properties)
            {
                var o = property.GetValue(info);
                o = o ?? DBNull.Value;
                cmd.Parameters.Add(new SqlParameter(property.Name, o));
            }
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        public static void Save(object info, SqlConnection conn, SqlTransaction tr)
        {
            var cmd = new SqlCommand(QuerySave(info), conn) {Transaction = tr};
            var properties = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
            foreach (var property in properties)
            {
                var o = property.GetValue(info);
                o = o ?? DBNull.Value;
                cmd.Parameters.Add(new SqlParameter(property.Name, o));
            }
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }


        public static void SaveList(List<object> modeles)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnection.GetConnection();
                SaveList(modeles, conn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, conn);
            }
        }

        public static void SaveList(List<object> modeles, SqlConnection conn)
        {
            if (modeles != null && modeles.Count != 0)
            {
                var sq = new SqlCommand(QuerySave(modeles[0]), conn);
                foreach (var modele in modeles)
                {
                    var properties = modele.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
                    foreach (var property in properties)
                    {
                        var o = property.GetValue(modele);
                        o = o ?? DBNull.Value;
                        sq.Parameters.Add(new SqlParameter(property.Name, o));
                    }

                    sq.ExecuteNonQuery();
                    sq.Parameters.Clear();
                }
                sq.Dispose();
            }
            else
            {
                throw new Exception("erreur save List");
            }
        }

        public static void Update(object info, string columnID)
        {
            SqlConnection con = null;
            try
            {
                con = DbConnection.GetConnection();
                Update(info, columnID, con);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, con);
            }
        }


        public static void Update(object info, string columnID, SqlConnection connection)
        {
            var propertyInfos = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
            var sqlm = new SqlCommand(QueryUpdate(info, columnID), connection);
            foreach (var property in propertyInfos)
            {
                var o = property.GetValue(info);
                o = o ?? DBNull.Value;
                sqlm.Parameters.Add(new SqlParameter(property.Name, o));
            }

            sqlm.ExecuteNonQuery();
            sqlm.Dispose();
        }

        public static void UpdateList(List<object> info, string columnID)
        {
            SqlConnection con = null;
            try
            {
                con = DbConnection.GetConnection();
                UpdateList(info, columnID, con);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, con);
            }
        }

        public static void UpdateList(List<object> bms, string columnID, SqlConnection con)
        {
            if (bms == null || bms.Count == 0) return;

            var sqlm = new SqlCommand(QueryUpdate(bms[0], columnID), con);
            foreach (var info in bms)
            {
                var propertyInfos = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
                foreach (var property in propertyInfos)
                {
                    var o = property.GetValue(info);
                    o = o ?? DBNull.Value;
                    sqlm.Parameters.Add(new SqlParameter(property.Name, o));
                }

                sqlm.ExecuteNonQuery();
                sqlm.Parameters.Clear();
            }

            sqlm.Dispose();
        }

        public static IEnumerable<object> Find(object info, SqlConnection conn, string where = "")
        {
            SqlDataReader sqd = null;
            var modeles = new List<object>();
            try
            {
                var properties = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual).ToArray();
                sqd = DbConnection.DataReader(QueryList(info, where), conn);

                while (sqd.Read())
                {
                    var modele = Activator.CreateInstance(info.GetType());
                    foreach (var property in properties)
                        if (sqd[property.Name].GetType() != typeof(DBNull))
                            property.SetValue(modele, sqd[property.Name]);

                    modeles.Add(modele);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(sqd, null);
            }

            return modeles;
        }

        public static List<object> FindQuery(string requette, object info, SqlConnection conn)
        {
            SqlDataReader sqd = null;
            var lmModeles = new List<object>();
            try
            {
                var propertyInfos = info.GetType().GetProperties().Where(p => !p.GetGetMethod().IsVirtual);
                sqd = DbConnection.DataReader(requette, conn);
                var properties = propertyInfos as PropertyInfo[] ?? propertyInfos.ToArray();
                while (sqd.Read())
                {
                    var modele = Activator.CreateInstance(info.GetType());
                    foreach (var property in properties)
                        if (sqd[property.Name].GetType() != typeof(DBNull))
                            property.SetValue(modele, sqd[property.Name]);

                    lmModeles.Add(modele);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(sqd, null);
            }

            return lmModeles;
        }

        public static IEnumerable<object> Find(object info, string where = "")
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnection.GetConnection();
                return Find(info, conn, where);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }
            finally
            {
                DbConnection.Close(null, conn);
            }
        }

        public static List<object> FindQuery(string requette, object info)
        {
            SqlConnection conn = null;
            try
            {
                conn = DbConnection.GetConnection();
                return FindQuery(requette, info, conn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                DbConnection.Close(null, conn);
            }
        }


        public static object First(object model)
        {
            var query = $"SELECT TOP 1 * FROM {model.GetType().Name}";
            return FindQuery(query, model).FirstOrDefault();
        }

        public static object FindByColumnGuid(object info, string columnId, Guid value)
        {
            var query = $"SELECT TOP 1 * FROM {info.GetType().Name} WHERE {columnId}='{value}'";
            var bms = FindQuery(query, info);
            return bms.Count != 0 ? bms[0] : null;
        }
    }
}