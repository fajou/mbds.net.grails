﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Front.Models.Base;
using Front.Models.Infos;
using Microsoft.EntityFrameworkCore.Internal;

namespace Front.Services.Implementation
{
    public class PretService : IPretService
    {
        private enum DurationType
        {
            Days, Months, Years
        }
        
        public Pret GetPret(Guid guid)
        {
            var pret = DataBase.FindByColumnGuid(new Pret(), "Id", guid);
            return pret as Pret;
        }

        private int GetDuration(DateTime start, DateTime end, DurationType type)
        {
            var duration = end - start;
            switch (type)
            {
                case DurationType.Days:
                    return duration.Days;
                case DurationType.Months:
                    return (end.Year - start.Year) * 12 + end.Month - start.Month;
                case DurationType.Years:
                    return end.Year - start.Year;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public PretConfig FindPretConfig(int duration, string durationType = "Days")
        {
            var config = new PretConfig();
            var where = $"{nameof(PretConfig.DurationMin)} <= {duration} AND {duration} <= {nameof(PretConfig.DurationMax)} AND {nameof(PretConfig.DurationType)} = '{durationType}'";
            return DataBase.Find(config, where).FirstOr(new PretConfig()) as PretConfig;
        }

        public PretConfig GetPretConfig(Guid guid)
        {
            var config = DataBase.FindByColumnGuid(new PretConfig(), "ConfigID", guid);
            return config as PretConfig;
        }

        public List<Pret> GetPrets(Client client)
        {
            var prets = DataBase.Find(new Pret(), $"ClientID='{client.Id}'");
            return prets.Cast<Pret>().ToList();
        }
        
        public Pret SavePret(PretInfo info)
        {
            var duration = GetDuration(info.DateStart, info.DateEnding, DurationType.Days);
            var pretConfig = FindPretConfig(duration);
            if (pretConfig.Id == Guid.Empty)
                throw new Exception("Nous n'avons pas encore cette offre pour le moment.");
            
            var pret = new Pret
            {
                Id = Guid.NewGuid(),
                PretConfigID = pretConfig.Id,
                ClientID = info.ClientID,
                CompteID = info.CompteID,
                Currency = CultureInfo.CurrentCulture.Name,
                Duration = duration,
                Description = info.Description,
                Date = DateTime.Now,
                StartDate = info.DateStart,
                EndDate = info.DateEnding,
                Valeurs = info.Values
            };
            DataBase.Save(pret);
            return pret;
        }

        public void SaveConfig(ConfigurationInfo info)
        {
           var configuration = new PretConfig
           {
               Id = Guid.NewGuid(),
               ConfigValues = info.ConfigValues,
               DurationMax = info.DurationMax,
               DurationMin = info.DurationMin,
               DurationType = info.DurationType
           };
           DataBase.Save(configuration);
        }
    }
}