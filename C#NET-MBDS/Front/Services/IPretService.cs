﻿using System;
using System.Collections.Generic;
using Front.Models.Base;
using Front.Models.Infos;

namespace Front.Services
{
    public interface IPretService : IService
    {
        Pret GetPret(Guid guid);

        PretConfig FindPretConfig(int duration, string durationType = "Days");

        PretConfig GetPretConfig(Guid guid);

        List<Pret> GetPrets(Client client);
        Pret SavePret(PretInfo info);
        void SaveConfig(ConfigurationInfo info);
    }
}