﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Front.Models.Base;
using Front.Models.Infos;

namespace Front.Services
{
    public interface IClientService : IService
    {
        Client GetClient(Guid guid);
        Client SaveClient(Client client);
        List<Client> GetClients();
        Client LoginClient(string mail, string pass);
        ClientInfo GetClientInfo(Guid guid);
        Collection<StateRemboursement> GetStateRemboursements(Client client, int year);
        void GenerateAllCompteClient();
        void GenerateManyPretsClient();
        void GenerateManyRemboursement();
    }
}