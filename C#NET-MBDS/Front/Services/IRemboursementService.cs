﻿using System;
using System.Collections.Generic;
using Front.Models.Base;

namespace Front.Services
{
    public interface IRemboursementService : IService
    {
        Remboursement GetRemboursement(Guid guid);
        List<RemboursementDetail> GetRemboursementDetails(Guid remboursementID);
        List<Remboursement> GetRemboursements(Pret pret, Client client);
        List<Remboursement> GetRemboursements(Client client);
        List<RemboursementClient> GetRemboursementClients(Client client);
    }
}