﻿using System;
using System.Collections.Generic;
using Front.Models.Base;

namespace Front.Services
{
    public interface ICompteService : IService
    {
        Compte GetCompte(Guid guid);

        List<Compte> GetComptesClient(Guid clientGuid);

        Compte CreateCompte(Client client, decimal? value);
    }
}