﻿using System;
using System.Globalization;
using System.Web.Mvc;
using BO.Models;
using BO.Services;
using BO.Services.Implementation;
using BO.ViewModels;

namespace BO.Controllers
{
    public class AdminController : Controller
    {
        private readonly IClientService ClientService;
        
        public AdminController(IClientService clientService)
        {
            ClientService = clientService;
        }

        // GET
        public ActionResult Login(string error = null)
        {
            ViewBag.Error = Equals(error, "true") ? "Invalide username or password, please try again!" : null;
            if (Session?["Client"] != null)
                return RedirectToAction("Index", "Back");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ValidationLogin(LoginViewModel login)
        {
            var client = ClientService.LoginClient(login.Username, login.Password);
            if (client?.Id == null)
            {
                const string error = "true";
                return RedirectToAction("Login", new {error});
            }

            Session["Client"] = client;
            return RedirectToAction("Index", "Back");
        }

        public ActionResult Register()
        {
            return View(new Client());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ValidationRegister(Client client)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Error = "Invalid content.";
                    return View("Register", client);
                }

                using (var context = new EF<Client>())
                {
                    client.Id = Guid.NewGuid();
                    client.Currency = CultureInfo.CurrentCulture.Name;
                    client.StartDate = DateTime.Now;
                    context.Save(client);
                    Session["Client"] = client;
                    return RedirectToAction("Index", "Back");
                }
            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message;
                return View("Register", client);
            }
        }

        public ActionResult Logout()
        {
            Session?.Clear();
            return RedirectToAction("Login");
        }
    }
}