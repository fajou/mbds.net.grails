﻿using System;
using System.Web.Mvc;
using BO.ViewModels;

namespace BO.Controllers
{
    /// <summary>
    ///     This controller is only use for the rendering page content at layout view or more view place.
    ///     Do not use this controller for operation or action name. Only used for the more page renderings.
    /// </summary>
    public class PagesController : Controller
    {
        /// <summary>
        ///     Rendering the footer page in layout or anothers pages indepandents.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Footer()
        {
            return PartialView("Footer/_FooterContent");
        }

        /// <summary>
        ///     Rendering the menus page content in layoutor or anothers pages indepandents.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Menus()
        {
            return PartialView("Menus/_MenusContent");
        }

        /// <summary>
        ///     Rendering the the header page in the layout or anothers pages indepandents.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Header()
        {
            return PartialView("Header/_HeaderNavBar");
        }

        /// <summary>
        ///     Rendering breadcrumb content specifique for all content body in layout identify by <see cref="Guid" />
        /// </summary>
        /// <returns></returns>
        public ActionResult Breadcrumb()
        {
            return PartialView("Header/_Breadcrumbs");
        }

        /// <summary>
        ///     Rendering paging content specifique for all content body List />
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Paging(int page, int size, int count)
        {
            return PartialView("_Paging", new PagingViewModel
            {
                PageActive = page,
                PageSize = size,
                ItemCount = count
            });
        }
    }
}