﻿using System;
using System.Web.Mvc;
using BO.Models;
using BO.Services;
using BO.ViewModels;

namespace BO.Controllers
{
    public class BackController : Controller
    {
        private readonly IClientService clientService;
        private readonly IPretService pretService;
        private readonly IRemboursementService remboursement;

        public BackController(IClientService clientService, IPretService pretService, IRemboursementService remboursement)
        {
            this.clientService = clientService;
            this.pretService = pretService;
            this.remboursement = remboursement;
        }

        public ActionResult Index(SearchViewModel search)
        {
            var active = Request?["active"] ?? string.Empty;
            return View(new IndexViewModel
            {
                Page = search.Page,
                Size = search.Size,
                PretConfig = new PretConfig(),
                Active = active,
                NewPrets = pretService.GetNewPrets(search.Page, search.Size),
                PretConfigs = pretService.GetPretConfigs(),
                Remboursements = remboursement.GetRembsDetail(search.Page, search.Size),
                CountPret = pretService.Count(),
                CountRembs = remboursement.Count()
            });
        }

        public ActionResult UserProfile(Guid guid)
        {
            var client = clientService.GetClient(guid);
            Console.WriteLine(client.Id);
            return View(new ClientViewModel
            {
                Client = clientService.GetClient(guid),
                Clients = clientService.GetClients()
            });
        }

        public ActionResult Sates()
        {
            var year = int.TryParse(Request["year"], out var y) ? y : 2020;
            return View("Statistiques", new StatesViewModel
            {
                Prets = pretService.GetStatePrets(year),
                Remboursements = remboursement.GetStateRembs(year),
                Interts = remboursement.GetStateInteret(year)
            });
        }

        public ActionResult Config(PretConfig config)
        {
            pretService.SavePretConfig(config);
            return RedirectToAction("Index", new {Active = "settings"});
        }
    }
}