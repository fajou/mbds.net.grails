﻿using System.Collections.Generic;

namespace BO.ViewModels
{
    public class StatesViewModel
    {
        public List<decimal> Remboursements { get; set; }
        public List<decimal> Prets { get; set; }
        public List<decimal> Interts { get; set; }
    }
}