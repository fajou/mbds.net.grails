﻿using System;

namespace BO.ViewModels
{
    public class SearchViewModel
    {
        public int Page { get; set; } = 1;
        public int Size { get; set; } = 20;
        public string Active { get; set; }
        public string ClientName { get; set; }
        public string NumCompte { get; set; }
        public string Duration { get; set; }
        public DateTime Date { get; set; }
        public int Nouveau { get; set; }
        public int Encours { get; set; }
        public int Rembourse { get; set; }
    }
}