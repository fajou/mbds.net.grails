﻿namespace BO.ViewModels
{
    public class PagingViewModel
    {
        public int PageActive { get; set; } = 1;
        public int PageSize { get; set; } = 15;
        public int ItemCount { get; set; }
    }
}