﻿using System.Collections.Generic;
using BO.Models;

namespace BO.ViewModels
{
    public class IndexViewModel
    {
        public int Size { get; set; }
        public int Page { get; set; }
        public List<Pret> NewPrets { get; set; }
        public List<RembsDetail> Remboursements { get; set; }
        public PretConfig PretConfig { get; set; }
        public List<PretConfig> PretConfigs { get; set; }
        public string Active { get; set; }
        public int CountPret { get; set; }
        public int CountRembs { get; set; }

    }
}