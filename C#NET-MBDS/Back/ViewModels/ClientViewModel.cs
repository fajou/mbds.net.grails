﻿using System.Collections.Generic;
using BO.Models;

namespace BO.ViewModels
{
    public class ClientViewModel
    {
        public Client Client { get; set; }
        public List<Client> Clients { get; set; }
    }
}