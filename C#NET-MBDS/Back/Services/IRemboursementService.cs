﻿using System.Collections.Generic;
using BO.Models;

namespace BO.Services
{
    public interface IRemboursementService : IService
    {
        List<Remboursement> GetRemboursements(int page, int count);

        List<RembsDetail> GetRembsDetail(int page, int count);
        List<decimal> GetStateRembs(int year);
        decimal GetStateRembs(int year, int month);
        
        List<decimal> GetStateInteret(int year);
        decimal GetStateInteret(int year, int month);
    }
}