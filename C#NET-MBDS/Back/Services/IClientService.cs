﻿using System;
using System.Collections.Generic;
using BO.Models;

namespace BO.Services
{
    public interface IClientService : IService
    {
        Client GetClient(Guid guid);
        Client SaveClient(Client client);
        List<Client> GetClients();
        Client LoginClient(string mail, string pass);
    }
}