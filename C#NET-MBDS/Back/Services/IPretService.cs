﻿using System.Collections.Generic;
using BO.Models;

namespace BO.Services
{
    public interface IPretService : IService
    {
        List<Pret> GetNewPrets(int page, int count);

        PretConfig SavePretConfig(PretConfig config);
        List<PretConfig> GetPretConfigs(int page = 1, int count = 15);
        decimal GetStatePrets(int year, int month);
        List<decimal> GetStatePrets(int year);
    }
}