﻿﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BO.Services
{
    public interface IDataContext<TEntity> where TEntity : class
    {
        /// <summary>
        /// </summary>
        /// <param name="entity"></param>
        void Save(TEntity entity);

        /// <summary>
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        TEntity FindByID(Guid guid);

        List<TEntity> PageIn(int page, int size);
    }
}