﻿using System.Collections.Generic;
using System.Linq;
using BO.Models;

namespace BO.Services.Implementation
{
    public class RemboursementService : IRemboursementService
    {
        public List<Remboursement> GetRemboursements(int page, int count)
        {
            using (var db = new EF<Remboursement>())
            {
                page--;
                return db.Set<Remboursement>()
                    .Where(remb => remb.Version == 1)
                    .Skip(page * count)
                    .Take(count)
                    .ToList();
            }
        }

        public List<RembsDetail> GetRembsDetail(int page, int count)
        {
            using (var db = new EF<RembsDetail>())
            {
                page--;
                var rembs = db.Set<RembsDetail>()
                    .Where(detail => detail.Version == 1)
                    .Skip(page * count)
                    .Take(count)
                    .ToList();
                foreach (var remb in rembs)
                {
                    remb.Pret = db.Set<Pret>().FirstOrDefault(pret => pret.Id == remb.PretId);
                    remb.Client = db.Set<Client>().FirstOrDefault(client => client.Id == remb.ClientId);
                }

                return rembs;
            }
        }
        public decimal GetStateRembs(int year, int month)
        {
            using (var db = new EF<RemboursementDetail>())
            {
                var prets = db.Set<RemboursementDetail>();
                var data = (
                    from detail in prets
                    where detail.Date.Month == month && detail.Date.Year == year
                    select detail.Valeurs
                ).Sum();
                return data ?? 0;
            }
        }

        public List<decimal> GetStateRembs(int year)
        {
            var data = new List<decimal>();
            for (var month = 1; month <= 12; month++)
                data.Add(GetStateRembs(year, month));
            return data;
        }
        
        public decimal GetStateInteret(int year, int month)
        {
            using (var db = new EF<RemboursementDetail>())
            {
                var prets = db.Set<RemboursementDetail>();
                var data = (
                    from rembs in prets
                    where rembs.Date.Month == month && rembs.Date.Year == year
                    select rembs.Interet
                ).Sum();
                return data ?? 0;
            }
        }

        public List<decimal> GetStateInteret(int year)
        {
            var data = new List<decimal>();
            for (var month = 1; month <= 12; month++)
                data.Add(GetStateInteret(year, month));
            return data;
        }

        public int Count()
        {
            using (var app = new EF<RembsDetail>())
            {
                return app.Set<RembsDetail>().Count();
            }
        }
    }
}