﻿using System;
using System.Collections.Generic;
using System.Linq;
using BO.Models;

namespace BO.Services.Implementation
{
    public class PretService : IPretService
    {
        public List<Pret> GetNewPrets(int page, int count)
        {
            using (var db = new EF<Pret>())
            {
                page--;
                var prets = db.Set<Pret>()
                    .Skip(page)
                    .Take(count)
                    .OrderBy(pret => pret.Version)
                    .ToList();
                foreach (var pret in prets)
                {
                    pret.Client = db.Set<Client>().FirstOrDefault(client => client.Id == pret.ClientID);
                    pret.PretConfig = db.Set<PretConfig>().FirstOrDefault(config => config.Id == pret.PretConfigID);
                    pret.Compte = db.Set<Compte>().FirstOrDefault(compte => compte.Id == pret.CompteID);
                }

                return prets;
            }
        }

        public PretConfig SavePretConfig(PretConfig config)
        {
            using (var app = new EF<PretConfig>())
            {
                config.Id = Guid.NewGuid();
                config.ConfigValues /= 100;
                app.Save(config);
                return config;
            }
        }

        public List<PretConfig> GetPretConfigs(int page, int count)
        {
            using (var app = new EF<PretConfig>())
            {
                page--;
                return app.Set<PretConfig>()
                    .Skip(page)
                    .Take(count)
                    .ToList();
            }
        }

        public decimal GetStatePrets(int year, int month)
        {
            using (var db = new EF<Pret>())
            {
                var prets = db.Set<Pret>();
                var data = (
                    from pret in prets
                    where pret.StartDate.Month == month && pret.StartDate.Year == year
                    select pret.Valeurs
                ).Sum();
                return data ?? 0;
            }
        }

        public List<decimal> GetStatePrets(int year)
        {
            var data = new List<decimal>();
            for (var month = 1; month <= 12; month++) 
                data.Add(GetStatePrets(year, month));
            return data;
        }

        public int Count()
        {
            using (var app = new EF<Pret>())
            {
                return app.Set<Pret>().Count();
            }
        }
    }
}    