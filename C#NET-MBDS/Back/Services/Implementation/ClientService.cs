﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BO.Models;

namespace BO.Services.Implementation
{
    public class ClientService : IClientService
    {
        private static string Sha256Hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                return string.IsNullOrEmpty(value)
                    ? value
                    : string.Concat(hash
                        .ComputeHash(Encoding.UTF8.GetBytes(value))
                        .Select(item => item.ToString("x2"))
                    );
            }
        }

        public Client GetClient(Guid guid)
        {
            using (var app = new EF<Client>())
            {
                return app.FindByID(guid);
            }
        }

        public Client LoginClient(string mail, string pass)
        {
            using (var app = new EF<Client>())
            {
                var cryPass = Sha256Hash(pass);
                return app.Set<Client>().FirstOrDefault(client => Equals(mail, client.UserEmail) && Equals(cryPass, client.PassWord));
            }
        }

        public Client SaveClient(Client client)
        {
            using (var app = new EF<Client>())
            {
                app.Save(client);
                return client;
            }
        }

        public List<Client> GetClients()
        {
            using (var app = new EF<Client>())
            {
                var client = app.Set<Client>();
                return new List<Client>();
            }
        }

        public int Count()
        {
            using (var app = new EF<Client>())
            {
                return app.Set<Client>().Count();
            }
        }
    }
}