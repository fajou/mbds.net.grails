﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using BO.Models;
using Microsoft.EntityFrameworkCore;

namespace BO.Services.Implementation
{
    public sealed class EF<TEntity> : DbContext, IDataContext<TEntity> where TEntity : class
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
                return;

            var ConnectionString = ConfigurationManager.ConnectionStrings["ApplicationConnectionString"].ConnectionString;
            optionsBuilder.UseSqlServer(ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>();
            modelBuilder.Entity<Pret>();
            modelBuilder.Entity<PretConfig>();
            modelBuilder.Entity<Remboursement>();
            modelBuilder.Entity<RemboursementDetail>();
            modelBuilder.Entity<RembsDetail>();
            base.OnModelCreating(modelBuilder);
        }

        public void Save(TEntity entity)
        {
            Set<TEntity>().Add(entity);
            base.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            Set<TEntity>().Update(entity);
            base.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            Set<TEntity>().Remove(entity);
            base.SaveChanges();
        }

        public TEntity Find(TEntity entity)
        {
            return Set<TEntity>().Find(entity);
        }

        public TEntity FindByID(Guid guid)
        {
            return Set<TEntity>().FirstOrDefault();
        }

        public List<TEntity> PageIn(int page, int size)
        {
            var students = from s in Set<TEntity>() select s;
            return students.ToList();
        }
    }
}