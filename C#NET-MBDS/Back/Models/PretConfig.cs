using System;

namespace BO.Models
{
    public sealed class PretConfig
    {
        public Guid Id { get; set; }

        public decimal? ConfigValues { get; set; }
        
        public int DurationMin { get; set; }

        public int DurationMax { get; set; }
        
        public string DurationType { get; set; }
    }
}