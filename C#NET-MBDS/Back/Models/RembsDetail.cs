﻿using System;

namespace BO.Models
{
    public sealed class RembsDetail
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public DateTime Date { get; set; }
        public Decimal Interet { get; set; }
        public Decimal Valeurs { get; set; }
        public Int64 Version { get; set; }
        public Guid PretId { get; set; }
        public Guid ClientId { get; set; }
        public Decimal TotalValues { get; set; }
        public Decimal TotalLefts { get; set; }
        public Client Client { get; set; }
        public Pret Pret { get; set; }
        public int Etat { get; set; }
    }
}