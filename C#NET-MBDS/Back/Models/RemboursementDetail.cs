using System;

namespace BO.Models
{
    public sealed class RemboursementDetail
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public DateTime Date { get; set; }

        public decimal? Interet { get; set; }

        public decimal? Valeurs { get; set; }
        
        public Int64 Version { get; set; }
    }
}