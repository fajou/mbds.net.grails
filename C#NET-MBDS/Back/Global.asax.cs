﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BO
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            DependencyResolverConfig.Register();
//            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}