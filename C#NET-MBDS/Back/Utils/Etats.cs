﻿namespace BO.Utils
{
    public static class Etats
    {
        public static int NOUVEAU = 0;
        public static int EN_COURS = 1;
        public static int REMBOURSER = 2;
        public static int ANNULER = 3;
        
        public static string Status(int etat)
        {
            if (etat == EN_COURS)
                return "<code style='color:blue'>En cours</code>";
            if (etat == REMBOURSER)
                return "<code style='color:green'>Remboursé</code>";
            if (etat == ANNULER)
                return "Annulée";
            return etat == NOUVEAU ? "<code style='color:#fd7e14'>Nouveau</code>" : "";
        }
    }
}