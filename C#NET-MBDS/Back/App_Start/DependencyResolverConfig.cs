﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using BO.Services;

namespace BO
{
    /// <summary>
    ///     Registers required implementations to the Autofac container and set the container as ASP.NET MVC dependency resolver
    /// </summary>
    public static class DependencyResolverConfig
    {
        public static void Register()
        {
            ConfigureDependencyResolverForMvcApplication(new ContainerBuilder());
        }


        private static void ConfigureDependencyResolverForMvcApplication(ContainerBuilder builder)
        {
            // Enable property injection in view pages
            builder.RegisterSource(new ViewRegistrationSource());
            // Register web abstraction classes
            builder.RegisterModule<AutofacWebTypesModule>();
            // Register controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            // Register services
            builder.RegisterAssemblyTypes(typeof(MvcApplication).Assembly)
                .Where(x => x.IsClass && !x.IsAbstract && typeof(IService).IsAssignableFrom(x))
                .AsImplementedInterfaces()
                .InstancePerRequest();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}