﻿using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace BO.Extensions
{
    public static class HtmlHelperExtension
    {
        public static MvcHtmlString FooterPage(this HtmlHelper helper)
        {
            return helper.Action("Footer", "Pages");
        }

        public static MvcHtmlString MenusPage(this HtmlHelper helper)
        {
            return helper.Action("Menus", "Pages");
        }

        public static MvcHtmlString HeaderPage(this HtmlHelper helper)
        {
            return helper.Action("Header", "Pages");
        }

        public static MvcHtmlString Breadcrumb(this HtmlHelper helper)
        {
            return helper.Action("Breadcrumb", "Pages");
        }

        public static MvcHtmlString Paging(this HtmlHelper helper, int page, int size, int count)
        {
            return helper.Action("Paging", "Pages", new {page, size, count});
        }
    }
}