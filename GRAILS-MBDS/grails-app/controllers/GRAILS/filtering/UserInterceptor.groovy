package GRAILS.filtering

import GRAILS.Client
import GRAILS.Compte
import info.ConstName


class UserInterceptor {
    def userService

    UserInterceptor() {
        matchAll ().except (controller: 'user', action: 'loginBo')
                .except (controller: 'user', action: 'loginFo')
                .except (controller: 'user', action: 'registerBo')
                .except (controller: 'user', action: 'registerFo')
                .except (controller: 'user', action: 'register')
                .except (controller: 'user', action: 'auth')
                .except (uri: "/favicon.ico")
    }

    private boolean isBackLogged() {
        def client = session [ConstName.SESS_BO] as Client
        def logged = client != null && controllerName == "back"
        if (logged)
            session [ConstName.SESS_BO] = userService.getClient (client.id)
        logged
    }

    private boolean isFrontLogged() {
        def client = session [ConstName.SESS_FO] as Client
        def logged = client != null && controllerName == "front"
        if (logged) {
            def compte = session [ConstName.SESS_CC] as Compte
            session [ConstName.SESS_FO] = userService.getClient (client.id)
            session [ConstName.SESS_CP] = userService.comptes (client)
            session [ConstName.SESS_CC] = compte == null
                    ? userService.getCurrentCompte (client, null)
                    : userService.getCurrentCompte (client, compte.id)
        }
        logged
    }

    private boolean loginPage() {
        if (controllerName == "front") {
            redirect (controller: "user", action: "loginFo")
            return false
        }

        if (controllerName == "back") {
            redirect (uri: "/bo-user-login")
            return false
        }
        return true
    }

    boolean before() {
        println (request.getRequestURL ().toString ())
        if (isBackLogged ()) {
            return true
        } else if (isFrontLogged ()) {
            return true
        }
        return loginPage ()
    }

    boolean after() {true}

    void afterView() {
        // no-op
    }
}
