package GRAILS

import info.ConstName

class FrontController {
    def userService
    def pretService

    private Client getCurrentClient() {
        def client = session.getAttribute (ConstName.SESS_FO) as Client
        Client.get (client.id)
    }

    def index(int max) {
        def client = getCurrentClient ()
        params.max = Math.min (max?:10,25)
        def prets
        def count
        if (params.etat != null) {
            def etat = params.int ('etat')
            prets = Pret.findAllByEtatAndClient (etat,client,params)
            count = Pret.countByEtatAndClient (etat,client)
        } else {
            prets = pretService.prets (client,params)
            count = Pret.countByClient (client)
        }
        [prets    :prets,
         client   :getCurrentClient (),
         pretCount:count]
    }

    def comptes() {
        [comptes:userService.comptes (getCurrentClient ())]
    }

    def preter() {
        def client = getCurrentClient ()
        [comptes:userService.comptes (client),client:client]
    }

    private static void configPret() {
        def config = new PretConfig ()
        config.id = UUID.randomUUID ()
        config.configValues = new BigDecimal (0.2)
        config.durationMin = 10
        config.durationMax = 30
        config.durationType = 'Days'
        config.save ()
    }

    def doPret() {
        try {
            def client = getCurrentClient ()
            def compteID = UUID.fromString (params.pretCompteID.toString ())
            def compte = userService.getCurrentCompte (client,compteID)
            def duration = params.duration.toString ()
            def result = pretService.savePret (params,compte,client,duration)
            if (result != null) {
                redirect (controller:"front",action:"index")
                return
            }
            redirect (controller:"front",action:"preter")
        } catch (def e) {
            println (e.message)
            redirect (controller:"front",action:"preter")
        }
    }

    def rembourser() {
        def client = getCurrentClient ()
        def ok = pretService.rembourser (client,UUID.fromString (params.pretid.toString ()))
        if (ok)
            redirect (controller:"front",action:"index")
        else
            redirect (controller:"front",action:"remboursements")
    }

    def rembourserTout() {
        def client = getCurrentClient ()
        pretService.rembourserTout (client)
        redirect (controller:"front",action:"index")
    }
}
