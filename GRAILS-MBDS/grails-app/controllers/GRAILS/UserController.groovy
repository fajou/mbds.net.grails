package GRAILS


import info.ConstName
import org.springframework.web.servlet.support.RequestContextUtils

class UserController {
    def userService

    def loginBo() {}

    def loginFo() {}

    def registerBo() {}

    def registerFo() {}

    def auth() {
        def client = userService.auth (request.getParameter ("mail"), request.getParameter ("pass"))
        def office = request.getParameter ("check")
        if (client != null) {
            if (office == "bo") {
                session.setAttribute (ConstName.SESS_BO, client)
                redirect (uri: "/bo-index")
                return
            }
            if (office == "fo"){
                session.setAttribute (ConstName.SESS_FO, client)
                redirect (uri: "/fo-index")
                return
            }
        }
        if (office == "bo")
            redirect (uri: "/bo-user-login")
        else
            redirect (uri: "/")
    }

    def register() {
        if (request.method.toUpperCase () == "POST") {
            def client = new Client (params)
            def currency = RequestContextUtils.getLocale (request)
            client.setCurrency (currency.toLanguageTag ())
            client.setVersion (params.version as Long)
            client = userService.register (client)
            if (params.check == "bo") {
                session.setAttribute (ConstName.SESS_BO, client)
                redirect (controller: "back", action: "index")
            } else if (params.check == "fo") {
                session.setAttribute (ConstName.SESS_FO, client)
                redirect (controller: "front", action: "index")
            }
            return
        }
        params.check == "fo" ? redirect (uri: "/") : redirect (uri: "bo-user-login")
    }

    def changeCompte() {
        String compteId = params.current_compte_id
        def client = session [ConstName.SESS_FO] as Client
        def compte = session [ConstName.SESS_CC] as Compte
        if (compteId != null) {
            def current = userService.getCurrentCompte (client, UUID.fromString (compteId))
            session [ConstName.SESS_CC] = current
            render (contentType: 'text/json') {
                [
                        'results': current.id,
                        'status' : current.id ? "OK" : "Nothing present"
                ]
            }
            return
        }
        session [ConstName.SESS_CC] = compte == null ? userService.getCurrentCompte (client, null) : compte
        render (contentType: 'text/json') {
            [
                    'results': compte.id,
                    'status' : compte.id ? "OK" : "Nothing present"
            ]
        }
    }

    def newCompte() {
        userService.newCompte (params)
        redirect ([controller: "front", action: "index"])
    }

    def deconnectionFo() {
        getSession ().setAttribute (ConstName.SESS_FO, null)
        redirect (controller:"user", action:"loginFo")
    }

    def deconnectionBo() {
        getSession ().setAttribute (ConstName.SESS_BO, null)
        redirect (controller:"user", action:"loginBo")
    }
}
