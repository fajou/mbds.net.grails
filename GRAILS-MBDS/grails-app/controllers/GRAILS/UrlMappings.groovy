package GRAILS

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/" (controller: "user", action: "loginFo")
        "/fo-index" (controller: "front", action: "index")
        "/fo-user-register" (controller: "user", action: "registerFo")
        "/fo-nouveau-compte" (controller: "front", action: "createCompte")
        "/fo-save-compte" (controller: "front", action: "createCompte")
        "/fo-nouveau-pret" (controller: "front", action: "preter")
        "/fo-traiter-pret" (controller: "front", action: "doPret")
        "/fo-change-compte" (controller: "user", action: "changeCompte")
        "/fo-remboursements" (controller: "front", action: "remboursements")

        "/bo-index" (controller: "back", action: "index")
        "/bo-clients" (controller: "back", action: "listClients")
        "/bo-comptes-clients" (controller: "back", action: "listComptes")
        "/bo-list-all-prets" (controller: "back", action: "list")
        "/bo-user-login" (controller: "user", action: "loginBo")
        "/bo-user-register" (controller: "user", action: "registerBo")

        "/user-auth" (controller: "user", action: "auth")
        "/user-register" (controller: "user", action: "register")
        "500" (view: '/error')
        "404" (view: '/notFound')
    }
}
