package GRAILS

class BackController {
    def compteService
    def pretService

    def index(int max) {
        params.max = Math.min (max?:10,25)
        def prets
        def count
        if (params.etat != null) {
            def etat = params.int ('etat')
            prets = Pret.findAllByEtat (etat,params)
            count = Pret.countByEtat (etat)
        } else {
            prets = pretService.allPrets (params)
            count = Pret.count
        }
        [prets    :prets,
         pretCount:count]
    }

    def profile() {
    }

    def listClients(int max) {
        params.max = Math.min (max?:10,25)
        [clients:Client.list (params),clientCount:Client.count]
    }

    def createCompte() {
        try {
            params.version = 0
            def compte = compteService.saveCompte (params,new BigDecimal (0))
            redirect (controller:"back",action:"listComptes",params:[clientID:compte.clientId])
        } catch (def e) {
            println (e)
            [compte:new Compte (),error:e.message]
        }
    }

    def listComptes(int max) {
        params.max = Math.min (max?:10,25)
        def clientId = params ['clientId']
        def count
        def comptes
        if (params ['etat'] != null) {
            def etat = params.int ('etat')
            comptes = Compte.findAllByEtat (etat,params)
            count = Compte.countByEtat (etat)
        } else {
            comptes = Compte.findAll (params)
            count = clientId == null
                    ?Compte.count
                    :Compte.countByClient (Client.get (UUID.fromString (clientId.toString ())))
        }
        [comptes    :comptes,
         client     :clientId == null?null:Client.get (UUID.fromString (clientId.toString ())),
         compteCount:count]
    }

    def validateCompte() {
        def compteid = params.compteID
        if (compteid == null) {
            redirect (controller:"back",action:"index")
        }
        compteService.validateCompte (UUID.fromString (compteid.toString ()))
        redirect (controller:"back",action:"listComptes")
    }

    def validerPret(String pretid,String clientid) {
        def pretuuid = UUID.fromString (pretid)
        def clientuuid = UUID.fromString (clientid)
        pretService.validatePret (pretuuid, clientuuid)
        return redirect (controller:"back",action:"index")
    }
}
