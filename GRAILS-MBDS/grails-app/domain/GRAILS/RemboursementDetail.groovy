package GRAILS

class RemboursementDetail {
    UUID id
    UUID parentId
    int etat
    Date date
    BigDecimal interet
    BigDecimal valeurs
    Remboursement remboursement

    static constraints = {

    }

    static mapping = {
        id name: 'id', generator: 'assigned'
        remboursement column: 'parentId', updateable: false, insertable: false
    }
}
