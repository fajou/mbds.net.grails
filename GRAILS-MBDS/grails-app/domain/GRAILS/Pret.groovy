package GRAILS

import info.Etat

class Pret {
    UUID id
    int duration
    int etat
    Date date
    Date startDate
    Date endDate
    BigDecimal valeurs
    String currency
    String description

    Client client
    Compte compte
    PretConfig config

    static constraints = {
    }

    static mapping = {
        id name: 'id', generator: 'assigned'
        client column: 'clientid', updateable: false, insertable: true
        compte column: 'compteid', updateable: false, insertable: true
        config column: 'pretconfigid', updateable: false, insertable: true
        sort date:"asc"
    }

    def status() {
        if(etat == Etat.EN_COURS)
            return "<code style='color:blue'>En cours</code>"
        if(etat == Etat.REMBOURSER)
            return "<code style='color:green'>Remboursé</code>"
        if(etat == Etat.ANNULER)
            return "Annulée"
        if(etat == Etat.NOUVEAU)
            return "<code style='color:#fd7e14'>Nouveau</code>"
        return ""
    }

    def aRembourser() {
       return etat == 1
    }
}
