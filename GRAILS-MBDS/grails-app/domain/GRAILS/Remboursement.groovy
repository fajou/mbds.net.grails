package GRAILS

class Remboursement {
    UUID id
    UUID pretId
    UUID clientId
    int etat
    BigDecimal totalValues
    BigDecimal totalLefts

    static constraints = {
    }

    static mapping = {
        id name: 'id', generator: 'assigned'
    }
}
