package GRAILS

class Client {
    UUID id
    String lastName
    String firstName
    String userEmail
    String currency
    String passWord
    Date startDate

    static constraints = {
    }

    static mapping = {
        id name: 'id', generator: 'assigned'
    }
}
