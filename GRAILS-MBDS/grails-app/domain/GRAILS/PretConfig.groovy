package GRAILS

class PretConfig {
    UUID id
    BigDecimal configValues
    int durationMin
    int durationMax
    String durationType

    static constraints = {
    }
    static mapping = {
        id name: 'id', generator: 'assigned'
    }
}
