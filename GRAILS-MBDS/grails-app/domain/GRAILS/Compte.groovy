package GRAILS

class Compte {
    UUID id
    int etat
    String number
    BigDecimal valeurs
    String currency
    Client client

    static constraints = {
    }

    static mapping = {
        id name: 'id', generator: 'assigned'
        client column: 'clientID', updateable: false, insertable: true
    }
}
