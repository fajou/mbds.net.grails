package GRAILS

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import info.Status

@Transactional
class CompteService {
    def userService

    private static def random = new Random ()

    private static String slot(int min,int max) {
        def val = random.nextInt (max - min) + min
        String.valueOf (val)
    }

    String generateCompteNumber() {
        String slotI = slot (1000,5000)
        String slotII = slot (2000,6000)
        String slotIII = slot (7000,9000)
        String slotIV = slot (1000,9000)
        return String.format ("%s %s %s %s",slotI,slotII,slotIII,slotIV)
    }

    Compte saveCompte(Map params,BigDecimal values) {
        def clientid = params ['clientId']
        if (clientid == null)
            return new Compte (params)
        def uuid = UUID.fromString (clientid.toString ())
        def compte = new Compte (params)
        def number = generateCompteNumber ()
        def client = userService.getClient (uuid)

        compte.setId (UUID.randomUUID ())
        compte.setNumber (number)
        compte.setValeurs (values)
        compte.setCurrency (client?.currency)
        compte.setClient (client)
        compte.save (true)
    }

    Compte newCompte(GrailsParameterMap params,BigDecimal values) {
        params.version = 1
        saveCompte (params,values)
    }

    def validateCompte(UUID compteid) {
        def compte = Compte.findById (compteid)
        compte.etat = Status.VALID.ordinal ()
        compte.save (flush:true)
    }
}
