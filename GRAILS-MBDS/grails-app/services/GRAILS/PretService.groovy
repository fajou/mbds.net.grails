package GRAILS

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.time.TimeCategory
import info.Etat

import java.text.SimpleDateFormat

@Transactional
class PretService {

    private static Date toDate(String date) {
        def pattern = "dd/MM/yyyy"
        def simple = new SimpleDateFormat (pattern)
        return simple.parse (date)
    }

    private static PretConfig getPretConfig(Pret pret) {
        def where = PretConfig.where {
            durationType == "Days"
            durationMin <= pret.duration
            durationMax >= pret.duration
        }
        where.first ()
    }

    private static void saveRembsDetails(def rembsId,def valeurs,def interet) {
        def details = new RemboursementDetail ()
        details.version = 1
        details.id = UUID.randomUUID ()
        details.etat = Etat.REMBOURSER
        details.date = new Date()
        details.interet = interet
        details.valeurs = valeurs
        details.parentId = rembsId
        details.save (false)
    }

    private static int calculDuration(Pret pret,String duration) {
        def durations = duration.split ('-')
        if (durations.length != 2)
            return 0
        pret.startDate = toDate (durations [0].trim ())
        pret.endDate = toDate (durations [1].trim ())
        use (TimeCategory) {
            def result = pret.endDate - pret.startDate
            println "days: ${result.days}, Hours: ${result.hours}"
            return result.days
        }
    }

    Pret savePret(GrailsParameterMap params,Compte compte,Client client,String durre) {
        try {
            def pret = new Pret (params)
            pret.duration = calculDuration (pret,durre)
            pret.currency = client.currency
            pret.version = 1
            pret.etat = Etat.NOUVEAU
            pret.id = UUID.randomUUID ()
            pret.date = new Date ()
            def config = getPretConfig (pret)

            pret.setConfig (config)
            pret.setClient (client)
            pret.setCompte (compte)
            pret.save (false)
        } catch (def e) {
            println (e.message)
            null
        }
    }

    List<Pret> prets(Client client,Map map) {
        return Pret.findAllByClient (client,map)
    }

    List<Pret> allPrets(Map map) {
        return Pret.findAll (map)
    }

    List<Remboursement> remboursements(Map map) {
        return Remboursement.findAll (map)
    }

    def rembourser(Client client,UUID pretid) {
        try {
            def pret = Pret.get (pretid)
            def compte = Compte.findByClient (client)
            def rembs = Remboursement.findByClientIdAndPretId (client.id,pret.id)
            if (rembs != null) {
                rembs.etat = Etat.REMBOURSER
                rembs.totalLefts = 0
                rembs.save (false)
                pret.etat = Etat.REMBOURSER
                compte.valeurs = compte.valeurs - rembs.totalValues
                compte.save (false)
                pret.save (false)
                saveRembsDetails (rembs.id,rembs.totalValues,pret.config.configValues * rembs.totalValues)
                return true
            }
            return false
        }
        catch (e) {
            println e.message
            return false
        }
    }

    def rembourserTout(Client client) {
        def prets = Pret.findAllByClientAndEtat (client, Etat.EN_COURS)
        prets.each {def pret ->
            def ok = rembourser (client,pret.id)
            if (!ok) {
                println "Not ok pretId:" + pret.id
            }
        }
    }

    def validatePret(UUID uuid, UUID clientId) {
        def pret = Pret.get (uuid)
        pret.etat = Etat.EN_COURS
        def rembs = Remboursement.findByClientIdAndPretId (clientId,pret.id)
        if (rembs == null) {
            rembs = new Remboursement ()
            rembs.id = UUID.randomUUID ()
            rembs.version = 0
            rembs.etat = Etat.REMBOURSER
            rembs.clientId = clientId
            rembs.pretId = pret.id
            rembs.totalLefts = pret.valeurs
            rembs.totalValues = pret.valeurs
            rembs.save (false)
            pret.save (false)
        }
    }
}
