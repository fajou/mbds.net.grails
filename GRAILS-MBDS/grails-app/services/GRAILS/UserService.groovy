package GRAILS

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class UserService {
    def compteService

    Client getClient(UUID uuid) {
        Client.get (uuid)
    }

    Client auth(String mail,String pass) {
        if (pass == null)
            return null as Client
        Client.findByUserEmailAndPassWord (mail,pass.encodeAsSHA256 ().toString ())
    }

    Client register(Client client) {
        client.setStartDate (new Date ())
        client.setId (UUID.randomUUID ())
        client.setPassWord (client.passWord.encodeAsSHA256 ().toString ())
        return client.save (true)
    }

    List<Compte> comptes(Client client) {
        if (client == null)
            new ArrayList<Compte> ()
        def val = Compte.findAllByClient (client)
        val
    }

    Compte getCurrentCompte(Client client,UUID uuid) {
        if (uuid == null)
            return Compte.findByClient (client)
        return Compte.get (uuid)
    }

    def newCompte(GrailsParameterMap params) {
        compteService.newCompte (params,new BigDecimal (0))
    }
}
