package info

class Etat {
    public final static int NOUVEAU = 0
    public final static int EN_COURS = 1
    public final static int REMBOURSER = 2
    public final static int ANNULER = 3
}
