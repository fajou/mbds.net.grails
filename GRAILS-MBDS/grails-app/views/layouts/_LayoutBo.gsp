<%@ page import="info.Breadcrumb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Bank-DS | Dashboard 2</title>
    <asset:stylesheet rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"/>
    <asset:stylesheet rel="stylesheet" href="dist/css/adminlte.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style>
.xs {
    font-size: .90rem !important;
}
</style>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed xs">
<div class="wrapper">
    <g:render template="/back/navPage"/>
    <g:render template="/back/leftPage"/>
    <div class="content-wrapper">
        <section class="content-header">
            <g:render template="/back/breadCrumb" var="breadcrumb" model="[breadcrumb:new info.Breadcrumb ()]"/>
        </section>
        <section class="content">
            <div class="container-fluid">
                <g:layoutBody/>
            </div>
        </section>
    </div>
    <g:render template="/back/footerPage"/>
</div>
<asset:javascript src="plugins/jquery/jquery.min.js"/>
<asset:javascript src="plugins/bootstrap/js/bootstrap.bundle.min.js"/>
<asset:javascript src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"/>
<asset:javascript src="dist/js/adminlte.js"/>
%{--<asset:javascript src="dist/js/demo.js"/>--}%

<asset:javascript src="plugins/jquery-mousewheel/jquery.mousewheel.js"/>
<asset:javascript src="plugins/raphael/raphael.min.js"/>
<asset:javascript src="plugins/jquery-mapael/jquery.mapael.min.js"/>
<asset:javascript src="plugins/jquery-mapael/maps/usa_states.min.js"/>
<asset:javascript src="plugins/chart.js/Chart.min.js"/>
<asset:javascript src="dist/js/dashboard2.js"/>

</body>
</html>
