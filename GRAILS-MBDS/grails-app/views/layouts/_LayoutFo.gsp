<%@ page import="info.Breadcrumb; info.ClientInfo" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <title>Bank-DS | Top Navigation</title>
    <asset:stylesheet href="plugins/fontawesome-free/css/all.min.css" rel="styleshets"/>
    <asset:stylesheet rel="stylesheet" href="bootstrap.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/select2/css/select2.min.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css"/>
    <asset:stylesheet href="dist/css/adminlte.css" rel="styleshets"/>
    <asset:stylesheet href="Giant.css" rel="styleshets"/>
    <asset:javascript src="plugins/jquery/jquery.min.js"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="styleshets"/>
</head>
<style>
    .xs {
        font-size: .90rem !important;
    }
    .card{
        box-shadow: none;
        border: 1px solid #d2d6de;
    }
</style>

<body class="hold-transition layout-top-nav xs">
<div class="wrapper">
    <g:render template="/front/navPage"/>
    <div class="content-wrapper">
        <div class="content-header">
            <g:render template="/front/breadCrumb" model="${[breadcrumb:new info.Breadcrumb ()]}"/>
        </div>

        <div class="content">
            <div class="container">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3">
                            <g:render template="/front/profile" model="${new ClientInfo ()}"/>
                        </div>

                        <div class="col-md-9">
                            <g:layoutBody/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <g:render template="/front/footerPage"/>
</div>
<asset:javascript src="application.js"/>
<asset:javascript src="plugins/bootstrap/js/bootstrap.bundle.min.js"/>
<asset:javascript src="dist/js/adminlte.min.js"/>
<asset:javascript src="plugins/bootstrap/js/bootstrap.bundle.min.js" asp-append-version="true"/>
<asset:javascript src="plugins/select2/js/select2.full.min.js" asp-append-version="true"/>
<asset:javascript src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js" asp-append-version="true"/>
<asset:javascript src="plugins/moment/moment.min.js" asp-append-version="true"/>
<asset:javascript src="plugins/inputmask/min/jquery.inputmask.bundle.min.js" asp-append-version="true"/>
<asset:javascript src="plugins/daterangepicker/daterangepicker.js" asp-append-version="true"/>
<asset:javascript src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js" asp-append-version="true"/>
<asset:javascript src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"
                  asp-append-version="true"/>
<asset:javascript src="plugins/bootstrap-switch/js/bootstrap-switch.min.js" asp-append-version="true"/>

<script>
    $(function () {
        //Initialize Select2 Elements   
        $('.select2').select2();

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/MM/yyyy', {'placeholder': 'dd/mm/yyyy'});
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('dd/MM/yyyy', {'placeholder': 'dd/mm/yyyy'});
        //Money Euro
        $('[info-mask]').inputmask();

        //Date range picker
        $('#duration-id').daterangepicker({
            timePicker: false,
            timePickerIncrement: 30,
            minDate: '<g:formatDate date="${new Date()}" format="dd/MM/yyyy"/>',
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    })
</script>
</body>
</html>
