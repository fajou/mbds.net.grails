<%--
  Created by IntelliJ IDEA.
  User: ofanomez
  Date: 12/11/2019
  Time: 3:46 PM
--%>

<%@ page import="GRAILS.Compte; GRAILS.Client" contentType="text/html;charset=UTF-8" %>
<meta name="layout" content="_LayoutFo">

<div class="invoice mb-4">
    <div class="card-header p-2">
        <h4>Demander un nouveau pret.</h4>
    </div>

    <div class="card-body">
        <div class="tab-content">
            <div class="active tab-pane" id="activity">
                <div class="row">
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-8">
                        <g:form method="post" controller="front" action="doPret">
                            <div class="invoice">
                                <div class="card-body p-4">
                                    <h4>
                                        <small class="float-right" style="font-size: 12px;"><g:formatDate
                                                date="${new Date ()}" format="dd MMMM yyyy"/></small>
                                    </h4>

                                    <div class="form-group">
                                        <label for="mes-compte-id">Choisir un compte*</label>
                                        <g:select optionKey="id"
                                                  optionValue="number"
                                                  name="pretCompteID"
                                                  from="${comptes}"
                                                  id="mes-compte-id"
                                                  class="form-control select2"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="duration-id">Duration*:</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar-alt"></i>
                                                </span>
                                            </div>
                                            <input type="text" name="duration" data-date-format="dd-MM-yyyy"
                                                   class="form-control float-right" id="duration-id">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputClientCompany">Valeur à preter*</label>
                                        <input type="number" name="valeurs" value="0" id="inputClientCompany"
                                               class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="inputDescription">Description du pret*</label>
                                        <textarea name="description" required="required" id="inputDescription"
                                                  class="form-control" rows="4"></textarea>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" name="preter"
                                            class="btn-sm btn btn-danger float-right">Envoyer</button>
                                </div>
                            </div>
                        </g:form>
                    </div>

                    <div class="col-md-2">
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="timeline">
                <div class="row">
                    <div class="col-5 form-group">
                        <label for="inputSpentBudget">Date de remboursement</label>
                        <input type="text" disabled="" value="12/13/2019" id="inputSpentBudget" class="form-control">
                    </div>

                    <div class="col-5 form-group">
                        <label for="inputEstimatedDuration">Valeurs à rembourser</label>
                        <input type="number" id="inputEstimatedDuration" class="form-control">
                    </div>

                    <div class="col-2 form-group  text-center" style="margin-top:35px;">
                        <input type="submit" class="btn btn-sm btn-warning">
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-valign-middle">
                            <thead>
                            <tr>
                                <th style="width: 10px;">****</th>
                                <th>Utitlisateur</th>
                                <th>Total payé</th>
                                <th>Reste payé</th>
                                <th>Progression</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <a href="#" class="text-muted">
                                        <i class="fas fa-eye primary"></i>
                                    </a>
                                </td>
                                <td>
                                    <img src="/Content/dist/img/default-150x150.png" alt="Product 1"
                                         class="img-circle img-size-32 mr-2">
                                    Some Product
                                </td>
                                <td>$13 USD</td>
                                <td>$13 USD</td>
                                <td>
                                    <small class="text-success mr-1">
                                        <i class="fas fa-arrow-up"></i>
                                        12%
                                    </small>
                                    12,000 Sold
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="settings">
                <div class="timeline timeline-inverse">
                    <div>
                        <div class="timeline-item">
                            <span class="time"><i class="far fa-clock"></i> 12:05</span>

                            <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                            <div class="timeline-body">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                            </div>

                            <div class="timeline-footer">
                                <a href="#" class="btn btn-primary btn-sm">Validé</a>
                                <a href="#" class="btn btn-danger btn-sm">Refusé</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>