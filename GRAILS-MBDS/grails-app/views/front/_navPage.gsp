<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a class="navbar-brand" href="index3.html">
            <span class="brand-text font-weight-light">Bank-BOA</span>
        </a>
        <button aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"
                class="navbar-toggler order-1" data-target="#navbarCollapse" data-toggle="collapse"
                type="button">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <g:link class="nav-link" controller="front" action="index">Home</g:link>
                </li>
            </ul>
        </div>
    </div>
</nav>