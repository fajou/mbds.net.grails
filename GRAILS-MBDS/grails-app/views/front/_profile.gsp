<%@ page import="info.Etat; info.Status; info.ConstName; GRAILS.Pret; GRAILS.Remboursement; info.ClientInfo; GRAILS.Client; GRAILS.Compte" %>
<%
    def profile = session [ConstName.SESS_FO] as Client
    def nbPret = Pret.countByClient (profile)
    def nbRembs = Pret.countByEtatAndClient (Etat.EN_COURS,profile)
    def nbCompte = Compte.countByClientAndEtat (profile,Status.VALID.ordinal ())
    def rembs = Remboursement.findAllByClientId (profile.id)
    def comptes = Compte.findAllByClientAndEtat (profile, Etat.EN_COURS)
%>
<style>
    .list-group-item {
        padding: 0 !important;
    }
</style>
<div class="card card-primary card-outline">
    <div class="card-body box-profile">
        <div class="text-center">
            <asset:image class="profile-user-img img-fluid img-circle" src="dist/img/user4-128x128.jpg"
                         alt="User profile picture"/>
        </div>

        <h3 class="profile-username text-center">${profile.lastName}</h3>

        <p class="text-muted text-center">${profile.firstName}</p>
        <g:if test="${0 < nbCompte}">
            <div class="text-center mb-2">
                <g:link action="preter" controller="front" class="btn btn-default btn-xs">
                    <i class="fas fa-edit">Nouveau pret</i>
                </g:link>
                <g:if test="${nbRembs != 0}">
                    <g:link controller="front" action="rembourserTout"
                            class="btn btn-xs btn-outline-danger"><i
                            class="fa fa-tasks"></i> Rembourser Tout</g:link>
                </g:if>
            </div>
        </g:if>
        <div class="mb-3">
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Total solde</b>
                    <p><g:formatNumber number="${comptes.sum {it.valeurs}}" format="###,##0"/></p>
                </li>
                <g:if test="${0 < nbPret}">
                    <li class="list-group-item">
                        <b>Reste &agrave; rembourser</b>
                        <p><g:formatNumber number="${rembs.sum {it.totalLefts}}" format="###,##0"/></p>
                    </li>
                    <li class="list-group-item">
                        <b>Total prets</b>

                        <p><g:formatNumber number="${rembs.sum {it.totalValues}}" format="###,##0"/></p>
                    </li>
                    <li class="list-group-item">
                        <b>Total remboursement</b>
                        <p><g:formatNumber number="${rembs.sum {it.totalValues - it.totalLefts}}" format="###,##0"/></p>
                    </li>
                </g:if>
                <li class="m-3">
                    <b>Demander un compte</b>
                    <g:link action="newCompte" controller="user" params="[clientId:profile.id]"
                            class="btn btn-primary btn-xs float-right">
                        <i class="fas fa-newspaper"></i>
                    </g:link>
                </li>
            </ul>
        </div>

        <div class="text-center mb-2">
            <g:link action="deconnectionFo" controller="user" class="btn btn-default btn-sm">
                Deconnexion
            </g:link>
        </div>
    </div>
</div>
%{--netsh wlan set hostednetwork mode=allow ssid=SharedWIFI key=Ik12345!--}%