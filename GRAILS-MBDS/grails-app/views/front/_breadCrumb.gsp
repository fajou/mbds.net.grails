<div class="container">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h3 class="m-0 text-dark"><small>${breadcrumb.subtitle}</small></h3>
        </div>

        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">${breadcrumb.page}</a></li>
                <li class="breadcrumb-item"><a href="#">${breadcrumb.title}</a></li>
                <li class="breadcrumb-item active">${breadcrumb.subtitle}</li>
            </ol>
        </div>
    </div>
</div>