<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bank-DS | Registration Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <asset:stylesheet href="plugins/fontawesome-free/css/all.min.css" rel="styleshets"/>
    <asset:stylesheet rel="stylesheet" href="bootstrap.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css"/>
    <asset:stylesheet rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"/>
    <asset:stylesheet href="dist/css/adminlte.css" rel="styleshets"/>
    <asset:stylesheet href="Giant.css" rel="styleshets"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <a href="/"><b>Admin</b>LTE</a>
    </div>

    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Register a new membership</p>
            <g:form name="check_fo" url="[controler:'user',action:'register']" method="post">
                <input type="hidden" name="version" value="1">
                <div class="input-group mb-3">
                    <input name="lastName" type="text" class="form-control" placeholder="Last name">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input name="firstName" type="text" class="form-control" placeholder="Full name">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input name="userEmail" type="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <input name="passWord" type="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                            <label for="agreeTerms">
                                I agree to the <a href="#">terms</a>
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="submit" name="check" value="fo"
                                class="btn btn-primary btn-block btn-flat">Register</button>
                    </div>
                </div>
            </g:form>
            <hr/>
            <g:link action="loginFo" controller="user" class="text-center">I already have a membership</g:link>
        </div>
    </div>
</div>
<asset:javascript src="plugins/jquery/jquery.min.js"/>
<asset:javascript src="plugins/bootstrap/js/bootstrap.bundle.min.js"/>
<asset:javascript src="dist/js/adminlte.min.js"/>
</body>
</html>