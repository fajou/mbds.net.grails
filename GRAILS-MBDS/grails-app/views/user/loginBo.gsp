<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bank-DS | Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <asset:stylesheet rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css"/>
    <asset:stylesheet rel="stylesheet" href="bootstrap.css"/>
    <asset:stylesheet rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <asset:stylesheet rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.css"/>
    <asset:stylesheet href="dist/css/adminlte.css" rel="styleshets"/>
    <asset:stylesheet href="Giant.css" rel="styleshets"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>Back office - </b>Grails</a>
    </div>

    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <g:form name="check_bo" method="post" url="[controller:'user',action:'auth']">
                <div class="input-group mb-3">
                    <input name="mail" type="email" class="form-control" placeholder="Email">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <input name="pass" type="password" class="form-control" placeholder="Password">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>

                    <div class="col-4">
                        <button type="submit" name="check" value="bo"
                                class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
            </g:form>
            <hr/>

            <p class="mb-0">
                <g:link action="registerBo" controller="user" class="text-center">Register a new membership</g:link>
            </p>
            <p class="mb-0">
                <g:link action="loginFo" controller="user" class="text-center text-danger">Front office</g:link>
            </p>
        </div>
    </div>
</div>
<asset:javascript src="/plugins/jquery/jquery.min.js"/>
<asset:javascript src="/plugins/bootstrap/js/bootstrap.bundle.min.js"/>
<asset:javascript src="/dist/js/adminlte.min.js"/>
</body>
</html>
