<%@ page import="info.Status" %>
<%--
  Created by IntelliJ IDEA.
  User: ofanomez
  Date: 12/10/2019
  Time: 7:24 AM
--%>
<meta name="layout" content="_LayoutBo">
<g:form method="get" controller="back" action="listComptes">
    <div class="invoice">
        <div class="card-header p-2">
            <g:if test="${client != null}">
                <h4>Liste des comptes - ${client.firstName} ${client.lastName}</h4>
            </g:if>
            <g:else>
                <h4>Liste des comptes</h4>
            </g:else>
        </div>

        <div class="card-header">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-check">
                        <g:radio type="checkbox" name="etat" value="${Status.NEW.ordinal ()}"
                                 class="form-check-input"
                                 id="nouveauid"/>
                        <label class="form-check-label" for="nouveauid">Nouveau</label>
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-check">
                        <g:radio type="checkbox" name="etat" value="${Status.VALID.ordinal ()}"
                                 class="form-check-input"
                                 id="encoursid"/>
                        <label class="form-check-label" for="encoursid">En cours</label>
                    </div>
                </div>

                <div class="col-lg-1">
                    <button type="submit" class="btn btn-xs btn-info">Afficher</button>
                </div>
            </div>
        </div>

        <div class="card-body">

            <table class="table table-bordered table-responsive-sm">
                <tbody>
                <tr>
                    <th>N* de Compte</th>
                    <th>Comptable</th>
                    <th>Disponible</th>
                    <th>Devise</th>
                    <th style="width: 100px">Type</th>
                    <th style="width: 100px">Action</th>
                </tr>
                <g:each in="${comptes}">
                    <tr>
                        <td><b>${it.number}</b></td>
                        <td>${it.valeurs}</td>
                        <td>${it.valeurs}</td>
                        <td>${it.currency}</td>
                        <td>Visa</td>
                        <td>
                            <g:if test="${it.etat == Status.NEW.ordinal ()}">
                                <g:link action="validateCompte" controller="back" params="[compteID:it.id]"
                                        class="btn btn-danger btn-xs">Valider <i class="fa fa-check"></i></g:link>
                            </g:if>
                            <g:else>
                                En cours
                            </g:else>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            <g:paginate next="Suivant" prev="Prevu" maxsteps="0" controller="back" action="listComptes"
                        total="${compteCount}"/>
        </div>
    </div>
</g:form>