<%@ page import="info.Etat" contentType="text/html;charset=UTF-8" %>
<meta name="layout" content="_LayoutBo">

<div class="card">
    <div class="card-header p-2">
        <h4>Liste de tous les prets</h4>
    </div>
    <g:form method="get" controller="back" action="index">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-check">
                        <g:radio type="checkbox" name="etat" value="${Etat.NOUVEAU}"
                                 class="form-check-input"
                                 id="nouveauid"/>
                        <label class="form-check-label" for="nouveauid">Nouveau</label>
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-check">
                        <g:radio type="checkbox" name="etat" value="${Etat.EN_COURS}"
                                 class="form-check-input"
                                 id="encoursid"/>
                        <label class="form-check-label" for="encoursid">En cours</label>
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-check">
                        <g:radio type="checkbox" name="etat" value="${Etat.REMBOURSER}"
                                 class="form-check-input"
                                 id="rembourserid"/>
                        <label class="form-check-label" for="rembourserid">Rembouss&eacute;</label>
                    </div>
                </div>

                <div class="col-lg-1">
                    <button type="submit" class="btn btn-xs btn-info">Afficher</button>
                </div>
            </div>
        </div>
    </g:form>
    <div class="card-body">
        <table class="table table-bordered table-responsive-sm xs">
            <tr>
                <th>Client</th>
                <th>Debut - Fin</th>
                <th>Duration</th>
                <th>Valeurs</th>
                <th>N* Compte</th>
                <th>Status</th>
            </tr>
            <g:each in="${prets}">
                <tr>
                    <td>${it.client.firstName} ${it.client.lastName}</td>
                    <td><g:formatDate format="dd/MM/yyyy" date="${it.startDate}"/> - <g:formatDate format="dd/MM/yyyy"
                                                                                                   date="${it.endDate}"/></td>
                    <td>${it.duration} ${it.config.durationType}</td>
                    <td><g:formatNumber number="${it.valeurs}" format="###,##0"/></td>
                    <td>${it.compte.number}</td>
                    <td>
                        <g:if test="${it.etat == Etat.NOUVEAU}">
                            <g:link controller="back" action="validerPret" params="[pretid:it.id,clientid:it.client.id]"
                                    class="btn btn-xs btn-outline-danger"><i
                                    class="fa fa-edit"></i> Valider</g:link>
                        </g:if>
                        <g:else>
                            ${raw (it.status ())}
                        </g:else>
                    </td>
                </tr>
            </g:each>
        </table>
        <g:if test="${prets.size () == 0}">
            <p class="text-center">Aucun element</p>
        </g:if>
    </div>

    <div class="card-footer">
        <g:paginate prev="«" next="»" max="10" controller="back" action="index" total="${pretCount}"/>
    </div>
</div>