<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <g:link class="nav-link" controller="back" action="index">Prets</g:link>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <g:link class="nav-link" controller="back" action="listClients">Clients</g:link>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <g:link class="nav-link" controller="back" action="listComptes">Comptes</g:link>
        </li>
    </ul>
</nav>
