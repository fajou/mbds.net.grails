<%@ page import="info.ConstName; info.ClientInfo; GRAILS.Client" %>
<%
    def profile = session [ConstName.SESS_BO] as Client
%>

<aside class="main-sidebar sidebar-dark-primary elevation-0">
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">Bank-BOA</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <asset:image src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"/>
            </div>
            <div class="info">
                <a href="#" class="d-block">${profile.lastName} ${profile.firstName}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">MENUS</li>
            </ul>
        </nav>
    </div>
</aside>