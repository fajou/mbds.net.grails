<meta name="layout" content="_LayoutBo">

<div class="invoice">
    <div class="card-header p-2">
        <h4>Liste des clients</h4>
    </div>

    <div class="card-body">
        <table class="table table-bordered table-responsive-sm">
            <tbody>
            <tr>
                <th>Start Date</th>
                <th>Nom et pr&eacute;nom</th>
                <th>Adress E-mail</th>
                <th>Lang.</th>
                <th style="width: 100px">Comptes</th>
            </tr>
            <g:each in="${clients}">
                <tr>
                    <td><g:formatDate format="dd-MM-yyyy HH:mm" date="${it.startDate}"/></td>
                    <td>${it.firstName} ${it.lastName}</td>
                    <td>${it.userEmail}</td>
                    <td>${it.currency}</td>
                    <td>
                        <g:link action="createCompte" controller="back" params="${[clientId:it.id]}"
                                class="btn btn-default btn-xs"><i class="fa fa-plus"></i></g:link>
                        <g:link action="listComptes" controller="back" params="${[clientId:it.id]}"
                                class="btn btn-default btn-xs"><i class="fa fa-table"></i></g:link>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    <div class="card-footer">
        <g:paginate prev="«" next="»" max="10" controller="back" action="listClients" total="${clientCount}"/>
    </div>
</div>