<meta name="layout" content="_LayoutBo">

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header p-12">
                <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                    <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
                    <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
            </div>

            <div class="card-body p-2">
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Fixed Header Table</h3>

                                <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="table_search" class="form-control float-right"
                                               placeholder="Search">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body table-responsive p-0" style="height: 300px;">

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="timeline">

                    </div>

                    <div class="tab-pane" id="settings">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

